#! /usr/bin/gnuplot -persist
set terminal postscript eps enhanced
set output "./output.ps"
set encoding utf8
set xlabel "N"
set nokey
#set bmargin 4
set ylabel "deviation"
#set logscale x
set grid
#set xrange [-10: 10]
set yrange [-2: 2]
set mxtics 10

set title "Oscillators"
set datafile separator "\t"

plot "data.dat" using 1:2 with lines 
#plot "data.dat" using 1:3 with lines 
