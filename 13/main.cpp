#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define min(x, y) (x > y ? y : x)

typedef struct
{
    long double omega1;
    long double omega2;
    long double phi1;
    long double phi2;
} _state;

_state state;

long double dt = 1e-5;
long double beta = 10;
long double alpha = 0.005;

void iteration()
{
    long double delta_phi1 = dt * state.omega1;
    long double delta_phi2 = dt * state.omega2;
    long double delta_omega1 = dt * (beta / (1 - 2 * alpha) * ((alpha - 1) * state.phi1 - alpha * state.phi2));
    long double delta_omega2 = dt * (beta / (1 - 2 * alpha) * ((alpha - 1) * state.phi2 - alpha * state.phi1));

    state.phi1 += delta_phi1;
    state.phi2 += delta_phi2;
    state.omega1 += delta_omega1;
    state.omega2 += delta_omega2;
}

void print_state(unsigned iteration)
{
    printf("%d\t%llf\t%llf\t%llf\t%llf\t%llf\n", iteration, min(fabs(state.phi1 - state.phi2), fabs(state.phi2 + state.phi2)), state.omega1, state.omega2, state.phi1, state.phi2);
    fflush(stdout);
}

int main(int argc, char** argv)
{
    if(argc <= 4)
    {
        printf("Usage: %s speed1 speed2 pos1 pos2\n", argv[0]);
        exit(-1);
    }

    state.omega1 = atof(argv[1]);
    state.omega2 = atof(argv[2]);
    state.phi1 = atof(argv[3]);
    state.phi2 = atof(argv[4]);

    fprintf(stderr, "%llf\t%llf\t%llf\t%llf\n", state.omega1, state.omega2, state.phi1, state.phi2);

    for(unsigned i = 0; ; i++)
    {
        iteration();
        if(i % 1000000 == 0)
            print_state(i);
    }
}
